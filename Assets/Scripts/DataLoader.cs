﻿using System.Collections;
using UnityEngine;
using UnityEngine.Networking;

public class DataLoader : MonoBehaviour
{
    protected readonly string urlData = "https://s3-sa-east-1.amazonaws.com/static-files-prod/unity3d/models.json";
    public static int LayerMask { get; private set; }

    private void Awake()
    {
        LayerMask = UnityEngine.LayerMask.GetMask("Selectable");
        StartCoroutine(GetDataFromWeb());
    }

    protected IEnumerator GetDataFromWeb()
    {
        var request = UnityWebRequest.Get(urlData);
        yield return request.SendWebRequest();

        if (request.isHttpError || request.isNetworkError)
            UIBind.Instance.SetErrorLoading($"Error reading data from web! {request.error}");
        else
        {
            var root = JsonUtility.FromJson<Root>(
                System.Text.Encoding.UTF8.GetString(
                    request.downloadHandler.data,
                    3,
                    request.downloadHandler.data.Length - 3));

            if (root != null && root.models != null)
            {
                InstantiateGameObjects(root);
                UIBind.Instance.SetVisibleLoading(false);
            }
        }
    }


    private void InstantiateGameObjects(Root root)
    {
        short i = 0;
        foreach (var model in root.models)
        {
            var obj = (GameObject)Instantiate(Resources.Load($"3DModels/{model.name}"));
            obj.name = model.name;
            obj.layer = UnityEngine.LayerMask.NameToLayer("Selectable");

            obj.transform.position = new Vector3(model.position[0], model.position[1], model.position[2]);
            obj.transform.localScale = new Vector3(model.scale[0], model.scale[1], model.scale[2]);
            obj.transform.rotation = Quaternion.Euler(new Vector3(model.rotation[0], model.rotation[1], model.rotation[2]));
            obj.AddComponent<BoxCollider>();

            UIBind.Instance.AddObjectToListView(obj.name, i++);
        }
    }
}
