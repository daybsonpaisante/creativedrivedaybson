﻿using UnityEngine;

public class MeshModifier : MonoBehaviour
{
    private static Texture2D[] textures;
    private static Color[] colors;

    private void Awake()
    {
        textures = Resources.LoadAll<Texture2D>("Textures\\Skins");
        colors = new Color[15] { Color.red, Color.green, Color.blue,
                                 Color.cyan, Color.white, Color.yellow,
                                 Color.red, Color.blue, Color.cyan,
                                 Color.red, Color.yellow, Color.magenta,
                                 Color.green, Color.magenta, Color.blue };
    }

    public static void ChangeColor(GameObject o, short id, short sequence)
    {
        if (o != null)
            o.GetComponent<MeshRenderer>().material.color = colors[3 * id + sequence];
    }

    public static void ChangeTexture(GameObject o, short id, short sequence)
    {
        if (o != null)
            o.GetComponent<MeshRenderer>().material.mainTexture = textures[3 * id + sequence];
    }

    public static Texture2D getTexture(short id, short sequence)
    {
        return textures[3 * id + sequence];
    }

    public static Color getColor(short id, short sequence)
    {
        return colors[3 * id + sequence];
    }
}
