﻿using System;

[Serializable]
public class Root
{
    public Model[] models;
}

[Serializable]
public class Model
{
    public string name;
    public float[] position;
    public float[] rotation;
    public float[] scale;
}

