﻿using UnityEngine;
using UnityEngine.UI;

public class UIBind : MonoBehaviour
{
    static UIBind instance;
    public static UIBind Instance
    {
        get
        {
            if (instance == null)
            {
                var objs = FindObjectsOfType<UIBind>();
                if (objs.Length >= 1)
                {
                    instance = objs[0];
                }
            }
            return instance;
        }
    }

    public Button[] buttonsTexture;
    public Button[] buttonsColors;

    public Text TxtLoading;

    public GameObject Element3DLabelGroup;
    public Transform ContainerElement3DLabel;

    private void Awake()
    {
        var userInput = FindObjectOfType<UserInput>();

        buttonsColors[0].onClick.AddListener(() => MeshModifier.ChangeColor(userInput.Selected?.gameObject, userInput.index, 0));
        buttonsColors[1].onClick.AddListener(() => MeshModifier.ChangeColor(userInput.Selected?.gameObject, userInput.index, 1));
        buttonsColors[2].onClick.AddListener(() => MeshModifier.ChangeColor(userInput.Selected?.gameObject, userInput.index, 2));

        buttonsTexture[0].onClick.AddListener(() => MeshModifier.ChangeTexture(userInput.Selected?.gameObject, userInput.index, 0));
        buttonsTexture[1].onClick.AddListener(() => MeshModifier.ChangeTexture(userInput.Selected?.gameObject, userInput.index, 1));
        buttonsTexture[2].onClick.AddListener(() => MeshModifier.ChangeTexture(userInput.Selected?.gameObject, userInput.index, 2));
    }

    public void SetVisibleLoading(bool visible)
    {
        TxtLoading.enabled = visible;
    }

    public void SetErrorLoading(string message)
    {
        TxtLoading.enabled = true;
        TxtLoading.text = message;
    }

    public void AddObjectToListView(string name, short index)
    {
        var tx = Resources.Load<Texture2D>($"Textures/modelo0{index + 1}");
        var group = Instantiate(Element3DLabelGroup, ContainerElement3DLabel);
        group.GetComponentInChildren<Text>().text = name;
        group.GetComponentInChildren<RawImage>().texture = tx;
    }


    public void UpdateUIButtons(short id)
    {
        buttonsTexture[0].GetComponent<Image>().sprite = Sprite.Create(MeshModifier.getTexture(id, 0), new Rect(0, 0, 100, 100), Vector2.one / 2);
        buttonsTexture[1].GetComponent<Image>().sprite = Sprite.Create(MeshModifier.getTexture(id, 1), new Rect(0, 0, 100, 100), Vector2.one / 2);
        buttonsTexture[2].GetComponent<Image>().sprite = Sprite.Create(MeshModifier.getTexture(id, 2), new Rect(0, 0, 100, 100), Vector2.one / 2);

        buttonsColors[0].GetComponent<Image>().color = MeshModifier.getColor(id, 0);
        buttonsColors[1].GetComponent<Image>().color = MeshModifier.getColor(id, 1);
        buttonsColors[2].GetComponent<Image>().color = MeshModifier.getColor(id, 2);
         
    }
}
