﻿using System.Collections;
using System.IO;
using System.Linq;
using UnityEngine;

public class UserInput : MonoBehaviour
{
    public float upSpeed;
    public float rotateSpeed;
    public float scaleSpeed;

    private Transform selected;
    public Transform Selected => selected;
    Vector3 lastMouseCoordinate = Vector3.zero;

    public short index = 0;

    public void Salvar()
    {
        StartCoroutine(Save());
    }

    public IEnumerator Save()
    {
        var r = new Root();
        var objs = FindObjectsOfType<GameObject>().Where(e => e.layer == LayerMask.NameToLayer("Selectable")).ToList();
        r.models = new Model[objs.Count];

        for (int i = 0; i < objs.Count; i++)
        {
            r.models[i] = new Model();
            r.models[i].name = objs[i].name;
            r.models[i].position = new float[3] { objs[i].transform.position.x, objs[i].transform.position.y, objs[i].transform.position.z };
            r.models[i].rotation = new float[3] { objs[i].transform.eulerAngles.x, objs[i].transform.eulerAngles.y, objs[i].transform.eulerAngles.z };
            r.models[i].scale = new float[3] { objs[i].transform.localScale.x, objs[i].transform.localScale.y, objs[i].transform.localScale.z };
        }
        var json = JsonUtility.ToJson(r, true);
        File.WriteAllText(Application.dataPath + "/savedData.json", json);
        UIBind.Instance.TxtLoading.text = "Salvo!";
        UIBind.Instance.TxtLoading.enabled = true;
        yield return new WaitForSeconds(1);
        UIBind.Instance.TxtLoading.enabled = false;
    }

    void Update()
    {

        if (Input.GetMouseButton(0))
        {
            var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out RaycastHit hitInfoObj, 9999f, DataLoader.LayerMask))
            {
                selected = hitInfoObj.transform;

                if (selected.name.Contains("01"))
                    index = 0;
                else if (selected.name.Contains("02"))
                    index = 1;
                else if (selected.name.Contains("03"))
                    index = 2;
                else if (selected.name.Contains("04"))
                    index = 3;
                else if (selected.name.Contains("05"))
                    index = 4;
                UIBind.Instance.UpdateUIButtons(index);
            }
        }


        if (selected != null)
        {
            #region MOVEMENT

            //MOVE (x, z)
            if (Input.GetMouseButton(0))
            {
                selected.Translate(new Vector3(Input.GetAxis("Mouse X"), 0, Input.GetAxis("Mouse Y")), Space.World);

                /*var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                if (Physics.Raycast(ray, out RaycastHit hitInfoFloor, 9999f, LayerMask.GetMask("Floor")))
                    selected.position = new Vector3(hitInfoFloor.point.x, selected.position.y, hitInfoFloor.point.z);*/
            }

            //MOVE (y)
            if (Input.mouseScrollDelta.y != 0)
            {
                selected.Translate(Vector3.up * Input.mouseScrollDelta.y * upSpeed);
            }

            //ROTATE
            if (Input.GetMouseButton(1))
            {
                Vector3 mouseDelta = Input.mousePosition - lastMouseCoordinate;
                lastMouseCoordinate = Input.mousePosition;

                selected.localScale += Vector3.one * mouseDelta.magnitude * Input.GetAxis("Mouse Y") * scaleSpeed;
            }

            //SCALE
            if (Input.GetMouseButton(2))
            {
                Vector3 mouseDelta = Input.mousePosition - lastMouseCoordinate;
                lastMouseCoordinate = Input.mousePosition;

                selected.rotation = Quaternion.Euler(0, selected.eulerAngles.y + mouseDelta.magnitude * Input.GetAxis("Mouse X") * rotateSpeed, 0);
            }

            #endregion

            //DUPLICATE
            if (Input.GetKey(KeyCode.LeftControl) && Input.GetKeyDown(KeyCode.D))
            {
                var size = selected.GetComponent<MeshRenderer>().bounds.size;
                var pos = new Vector3(selected.transform.position.x + size.x, selected.transform.position.y, selected.transform.position.z + size.z);
                var obj = Instantiate(selected, pos, selected.transform.rotation);
                UIBind.Instance.AddObjectToListView(selected.name, index);
            }
        }
    }
}
